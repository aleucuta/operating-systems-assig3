#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#define BASE_HEADER_LENGTH 8
#define SECTION_LENGTH 23
#define SECTION_S_OFFSET 15
#define SECTION_S_OFFSET_LENGTH 4
#define OUT_PIPE "RESP_PIPE_74123"
#define IN_PIPE "REQ_PIPE_74123"
#define ALIGN_SIZE 3072

key_t SHM_KEY = 10501;
unsigned shm_size;
void *sharedData =NULL;
int variant=74123;
int in_pipe,out_pipe = -1;
int shmId;
unsigned fileSize;
void *baseAddress=NULL;

float ceil_(float n){
    return (int)n+1;
}

int openPipes(){

    unlink("RESP_PIPE_74123");
    mkfifo(OUT_PIPE,0777);
    
    in_pipe = open(IN_PIPE,O_RDONLY);
    if(in_pipe == -1){
        return -1;    
    }

    out_pipe = open(OUT_PIPE,O_WRONLY);
    if(out_pipe == -1){
        return -2;
    }
    
    char s[7]="CONNECT";
    
    char size = 7;
    write(out_pipe,&size,sizeof(char));
    write(out_pipe,s,strlen(s));
    return 1;
}

int readString(char *pCharsBuffer,char *pStringSize){
    read(in_pipe,pStringSize,sizeof(char));
    read(in_pipe,pCharsBuffer,*pStringSize*sizeof(char));
    return 0;
}

int writeString(char string[200]){
    char size = strlen(string);
    write(out_pipe,&size,sizeof(char));
    write(out_pipe,string,sizeof(char)*size);
    return 1;   
}

int writeNumber(unsigned number){
    write(out_pipe,&number,sizeof(unsigned));
    return 1;
}

int readNumber(unsigned *pNumber){
    read(in_pipe,pNumber,sizeof(unsigned));
    return 1;
}

void ping(){
    writeString("PING");
    writeString("PONG");
    writeNumber(variant);
}

void createShm(){

    readNumber(&shm_size);
    writeString("CREATE_SHM");
    shmId = shmget(SHM_KEY, shm_size, IPC_CREAT | 0644);
    if(shmId < 0){
        writeString("ERROR");
        return;
    }
    sharedData = (char*)shmat(shmId,NULL,0);
    if(sharedData == (void*)-1){
        writeString("ALTERROR");
        return;
    }
    writeString("SUCCESS");
    return;
}

void writeToShm(){
    writeString("WRITE_TO_SHM");
    unsigned offset,data;
    readNumber(&offset);
    readNumber(&data);
    //printf("%d %d %p %p\n",offset,data,sharedData,sharedData + 3539649);
    if(offset > 0 && offset < shm_size-4) // && data > sharedData && data < sharedData + 3539649)
    {   
        memcpy(sharedData + offset,&data,sizeof(unsigned));
        //sharedData = data;
        writeString("SUCCESS");
        return;
    }
    writeString("ERROR");
    return;
}

void mapFile(){
    char buffer[512];
    char size = 0;
    writeString("MAP_FILE");
    readString(buffer,&size);
    buffer[(int)size] = '\0';
    int fd = -1;
    fd = open(buffer,O_RDONLY);
    if(fd == -1){
        writeString("ERROR");
        return;
    }
    fileSize = lseek(fd,0,SEEK_END);

    baseAddress = (void*)mmap(NULL,fileSize,PROT_READ,MAP_SHARED,fd,0);
    if(baseAddress == (void*)-1){
        writeString("ERROR");
        close(fd);
        return;
    }
    writeString("SUCCESS");
    return;
}

int readFromFileOffset(unsigned offset,unsigned no_of_bytes){
    
    if(!sharedData || sharedData == (void*)-1){
        return -3;
    }
    if(offset + no_of_bytes > fileSize){
        return -3;
    }
    
    memcpy(sharedData,baseAddress + offset,no_of_bytes);
      return 3;
}


int readFromFileSection(unsigned section_no,unsigned offset,unsigned no_of_bytes){
    
    
    unsigned parsingOffset = BASE_HEADER_LENGTH + (section_no-1)*SECTION_LENGTH + SECTION_S_OFFSET;
    unsigned sectionOffset;
   
    memcpy(&sectionOffset,baseAddress+parsingOffset,4);
     
    int err = readFromFileOffset(sectionOffset+offset,no_of_bytes);
    fflush(stdout);
    if(err>0){
        return 4;
    }   
    return -4;
}

int rva2va(unsigned relative_virtual_address,unsigned no_of_bytes){

    unsigned char nr_sections;
    memcpy(&nr_sections,baseAddress + BASE_HEADER_LENGTH - 1,1);
    unsigned parsing_offset = BASE_HEADER_LENGTH;
    unsigned section_number;
    for(section_number = 1;section_number <= nr_sections;section_number++){
        unsigned section_size;
        memcpy(&section_size,baseAddress+parsing_offset+19,4);
        if(section_size > relative_virtual_address)
            break;
        else{
            parsing_offset += SECTION_LENGTH;
            relative_virtual_address-=ceil_((float)section_size/ALIGN_SIZE) * ALIGN_SIZE;
        }
    }
    
    if(section_number == nr_sections + 1)
        return -5;
    if(readFromFileSection(section_number,relative_virtual_address,no_of_bytes) < 0)
        return -5;
    return 5;
}
void errorHandler(int err){
    switch(err){
        //Z+ good stuff
        //Z- bad stuff
        case 1:
            printf("SUCCESS");
            fflush(stdout);
            break;
        case -1:
            printf("ERROR\ncannot open the request pipe");
            fflush(stdout);
            break;
        case -2:
            printf("ERROR\ncannot create the response pipe");
            fflush(stdout);
            break;
        case -3:
            writeString("READ_FROM_FILE_OFFSET");
            writeString("ERROR");
            break;
        case 3:
            writeString("READ_FROM_FILE_OFFSET");
            writeString("SUCCESS");
            break;
        case -4:
            writeString("READ_FROM_FILE_SECTION");
            writeString("ERROR");
            break;
        case 4:
            writeString("READ_FROM_FILE_SECTION");
            writeString("SUCCESS");
            break;
        case -5:
            writeString("READ_FROM_LOGICAL_SPACE_OFFSET");
            writeString("ERROR");
            break;
        case 5:
            writeString("READ_FROM_LOGICAL_SPACE_OFFSET");
            writeString("SUCCESS"); 
            break;         
  
    }
}

int operateWithPipe(){
    char buffer[200];
    char size = 0;
    do{
        readString(buffer,&size);
        buffer[(int)size] = '\0';
        if(strcmp(buffer,"PING") == 0){
            ping();
           // continue;
        }
        if(strcmp(buffer,"CREATE_SHM")== 0){
            createShm();
            //continue;
        }
        if(strcmp(buffer,"WRITE_TO_SHM") == 0){
            writeToShm();
            //continue;
        }
        if(strcmp(buffer,"MAP_FILE") == 0){
            mapFile();
            //return 0;
        }
        if(strcmp(buffer,"READ_FROM_FILE_OFFSET") == 0){
            unsigned offset,no_of_bytes;
            readNumber(&offset);
            readNumber(&no_of_bytes);
            errorHandler(readFromFileOffset(offset,no_of_bytes));
        }
        if(strcmp(buffer,"READ_FROM_FILE_SECTION") == 0){
            unsigned section_no,offset,no_of_bytes;
            readNumber(&section_no);
            readNumber(&offset);
            readNumber(&no_of_bytes);
            errorHandler(readFromFileSection(section_no,offset,no_of_bytes));
        }
        if(strcmp(buffer,"READ_FROM_LOGICAL_SPACE_OFFSET") == 0){
            unsigned relative_virtual_address,no_of_bytes;
            readNumber(&relative_virtual_address);
            readNumber(&no_of_bytes);
            errorHandler(rva2va(relative_virtual_address,no_of_bytes));
        }
    }while(strcmp(buffer,"EXIT")!=0);
    shmdt(sharedData);
    shmctl(shmId, IPC_RMID,0);
    return 0;
}


int main(){

    int err = openPipes();
    errorHandler(err);
    operateWithPipe();
};