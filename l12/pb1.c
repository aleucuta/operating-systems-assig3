#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>

#define MAX_VAL 3000
#define KEY 1234
sem_t *parent_wrote;
sem_t *parent_can_write;

int main(int argc, char **argv)
{
    //int fd;
   // off_t size, i;
   // char *data = NULL;
    int child=-1;

    sem_unlink("parentsem");
    sem_unlink("childsem");

    child = fork();
    if(child == 0){
        int shmId = -1;
        int *pSharedInt = NULL;
        parent_wrote = sem_open("parentsem",O_CREAT,0600,0);
        parent_can_write = sem_open("childsem",O_CREAT,0600,1);
        sem_wait(parent_wrote);
        sem_post(parent_wrote);
        shmId = shmget(KEY,0,0);
        if(shmId < 0){
            perror("Could not aquire shm");
            return 1;
        }
        pSharedInt = (int*)shmat(shmId,NULL,0);
        do{
            sem_wait(parent_wrote);
            *pSharedInt = *pSharedInt + 1;
            printf("Child %d\n",*pSharedInt);
            sem_post(parent_can_write);
        }while(*pSharedInt < MAX_VAL-1);
        sem_post(parent_can_write);
        //close(fd);
        //shmdt(pSharedInt);
        exit(1);
    }
    else{
        int shmId = -1;
        int *pSharedInt = NULL;
        parent_wrote = sem_open("parentsem",O_CREAT,0600,0);
        parent_can_write = sem_open("childsem",O_CREAT,0600,1);

        shmId = shmget(KEY,sizeof(int),IPC_CREAT | 0600);
        if(shmId < 0){
            perror("Could not aquire shm");
            return 1;
        }
        pSharedInt = (int*)shmat(shmId,NULL,0);
        *pSharedInt = 0;
        while(*pSharedInt < MAX_VAL-1){
            sem_wait(parent_can_write);
            *pSharedInt = *pSharedInt + 1;
            printf("Parent %d\n",*pSharedInt);
            sem_post(parent_wrote);
        }
        sem_post(parent_wrote);
      //  close(fd);
        sem_unlink("parentsem");
        sem_unlink("childsem");
        shmdt(pSharedInt);
        shmctl(shmId ,IPC_RMID,0);
        
    }
    return 0;
}
