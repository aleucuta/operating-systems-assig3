#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHM_KEY 1984

int main(void) {
    int shmId;
    char *sharedChar = NULL;

    shmId = shmget(SHM_KEY, sizeof(char), IPC_CREAT | 0600);
    if(shmId < 0) {
        perror("Could not aquire shm");
        return 1;
    }
    sharedChar = (char*)shmat(shmId, NULL, 0);
    if(sharedChar == (void*)-1){
        perror("Could not attach to shm");
        return 1;
    }

    *sharedChar = 'A';
    while(*sharedChar == 'A') {
        printf("sharedChar: %c\n", *sharedChar);
        sleep(1);
    }
    printf("sharedChar new value: %c\n", *sharedChar);

    shmdt(sharedChar);
    sharedChar = NULL;
    shmctl(shmId, IPC_RMID, 0);

    return 0;
}
