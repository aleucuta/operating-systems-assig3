#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHM_KEY 1984

int main(void) {
    int shmId;
    char *sharedChar = NULL;

    shmId = shmget(SHM_KEY, 0, 0);
    if(shmId < 0) {
        perror("Could not aquire shm");
        return 1;
    }
    sharedChar = (char*)shmat(shmId, NULL, 0);
    if(sharedChar == (void*)-1){
        perror("Could not attach to shm");
        return 1;
    }

    printf("found sharedChar: %c\n", *sharedChar);
    *sharedChar = 'X';

    shmdt(sharedChar);
    sharedChar = NULL;

    return 0;
}
