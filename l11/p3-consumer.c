#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define FIFO_NAME "matematica"

int main(void)
{
    int fd = -1;
    //int x = 0;

    //open, read and close fifo
    while(1){
        fd = open(FIFO_NAME, O_RDONLY);
        if(fd == -1) {
            perror("Could not open FIFO for reading");
            return 1;
        }

        int x,y;
        char c;
        read(fd, &x, sizeof(x));
        read(fd, &c, sizeof(c));
        read(fd, &y,sizeof(y));
        printf("I just read %d%c%d\n",x,c,y);
        close(fd);

        float rez;
        switch(c){
            case '-':
                rez = x-y;
                break;
            case '+':
                rez = x+y;
                break;
            case '/':
                rez = x/y;
                break;
            case 'x':
                rez = x*y;
                break; 
            default:
                rez = 0;
                break;
        }   
        fd = open(FIFO_NAME,O_WRONLY);
        write(fd, &rez, sizeof(rez));
        printf("I just wrote rez %.2f\n",rez);
        close(fd);
    }
    return 0;
}
