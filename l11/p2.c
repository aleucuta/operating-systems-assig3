#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int main(void)
{
    int fd[2];

    if(pipe(fd) != 0) {
        perror("Could not create pipe");
        return 1;
    }

    if(fork() != 0) {
        //parent
        close(fd[0]); //no use for read end
        printf("Ce vrei sa scriu:");
        char s[50];
        scanf("%s",s);
        int lens = strlen(s);
        write(fd[1], s, lens);
        printf("Parent: wrote %s to pipe\n", s);
        close(fd[1]);
        wait(NULL);
    } else {
        //child
        close(fd[1]); //no use for write end
        char s[50];
        int i=0;
        do{
            read(fd[0],&s[i],1);
            i++;
        }while(s[i-1]!='\0');

        printf("Child: read %s from pipe\n", s);
        close(fd[0]);
    }

    return 0;
}
