#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>

sem_t *p1,*p2;

char c = 0;
    
int main(void)
{
    int fdP2C[2];
    //fdC2P[2];
    int i;
   // sem_close("nume1");
    //sem_close("nume2");
    p1 = sem_open("nume1",O_CREAT,0777,0);
    p2 = sem_open("nume2",O_CREAT,0777,0);
    //sem_init(&p2,0);
    if(pipe(fdP2C) != 0) {
        perror("Could not create pipes");
        return 1;
    }

    if(fork() != 0) {
        //parent
        c = 'a';
        for(i=0; i<10; i++) {
            write(fdP2C[1], &c, sizeof(c));
            sem_post(p1);

            sem_wait(p2);
            read(fdP2C[0], &c, sizeof(c));
            
            printf("Parent: %c\n", c);
            c++;
        }
        close(fdP2C[1]);
        wait(NULL);
    } else {
        //child
        //close(fdP2C[1]);
        for(i=0; i<10; i++) {
            sem_wait(p1);
            read(fdP2C[0], &c, sizeof(c));
            printf("Child: %c\n", c);
            c++;
            write(fdP2C[1], &c, sizeof(c));
            sem_post(p2);
            
        }
        close(fdP2C[0]);
        close(fdP2C[1]);
    }

    return 0;
}
