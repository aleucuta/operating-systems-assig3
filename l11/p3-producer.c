#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define FIFO_NAME "matematica"

int main(void)
{
    int fd = -1;
    //int x = 42;

    //create fifo
    unlink(FIFO_NAME);
    if(mkfifo(FIFO_NAME, 0600) != 0) {
        perror("Could not create pipe");
        return 1;
    }
    while(1){
    //open, write and close fifo
        fd = open(FIFO_NAME, O_WRONLY);
        if(fd == -1) {
            perror("Could not open FIFO for writing");
            return 1;
        }
        printf("Operatia:");
        int x,y;
        char op;
        scanf("%d%c%d",&x,&op,&y);
        printf("Writing operation %d%c%d to FIFO\n", x,op,y);
        write(fd, &x, sizeof(x));
        write(fd, &op,sizeof(op));
        write(fd, &y, sizeof(y));
        close(fd);
        fd = open(FIFO_NAME, O_RDONLY);

        float rez;
        read(fd,&rez,sizeof(rez));
        printf("Rezultatul este %.2f\n",rez);
    }
    return 0;
}
