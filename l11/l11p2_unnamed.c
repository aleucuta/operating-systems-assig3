#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int main(void)
{
    int fd[2];
    //int pipelen = 0;

    if(pipe(fd) != 0) {
        perror("Could not create pipe");
        return 1;
    }
    int childid = fork();
    if(childid != 0) {
        //parent
       // int status;
        for(;;)
        close(fd[1]);
        
    } else {
        //child
        close(fd[0]); //no use for read end
        int x = 1;
        while(1){
            write(fd[1],&x,sizeof(int));
            printf("Wrote %d:\n",x);
            x++;
            //printf("%d",pipelen);
        }
    }

    return 0;
}
